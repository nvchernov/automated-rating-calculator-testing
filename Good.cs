﻿using OpenQA.Selenium;
using OpenQA.Selenium.Remote;

namespace ArcTesting
{
    public class Good
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
        }

        private IWebElement item;

        public Good(string name, IWebElement item)
        {
            this.name = name;
            this.item = item;
        }

        public void Click()
        {
            item.Click();
        }
    }
}