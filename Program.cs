﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;

namespace ArcTesting
{
    class Program
    {
        static StringBuilder logStr = new StringBuilder();
        static void Main(string[] args)
        {
            int seed = 301119921;
            int userCount = 200;
            int pagePositionsCount = 10;

            Console.WriteLine("Введите seed");
            seed = int.Parse(Console.ReadLine());
            log(string.Format("seed {0}", seed));
            
            Console.WriteLine("Введите количество пользователей");
            userCount = int.Parse(Console.ReadLine());
            log(string.Format("Количество пользователей {0}", userCount));



            User user = new User("http://chernov.ivserv2.tmweb.ru/catalog/t-shirts/", seed);

            Console.WriteLine("Введите количество красных товаров");
            int redCount = int.Parse(Console.ReadLine());
            for (int i = 0; i < redCount; ++i)
            {
                Console.WriteLine("Введите номер страницы красного товара #" + (i + 1).ToString());
                int page = int.Parse(Console.ReadLine());
                Console.WriteLine("Введите позицию товара красного товара #" + (i + 1).ToString());
                int pos = int.Parse(Console.ReadLine());
                user.AddRedGood(page, pos);
            }

            Console.WriteLine("Введите количество синих товаров");
            int blueCount = int.Parse(Console.ReadLine());
            for (int i = 0; i < blueCount; ++i)
            {
                Console.WriteLine("Введите номер страницы синего товара #" + (i + 1).ToString());
                int page = int.Parse(Console.ReadLine());
                Console.WriteLine("Введите позицию товара синего товара #" + (i + 1).ToString());
                int pos = int.Parse(Console.ReadLine());
                user.AddBlueGood(page, pos);
            }



            log(string.Format("Тестирование начато {0}", DateTime.Now.ToString("yyyy.MM.dd_H:mm:ss")));

            for (int i = 0; i < userCount; ++i)
            {
                int page = 1;
                log(string.Format("Пользователь {0} посетил магазин", i + 1));
                while (user.WantToGoToNextPage(page))
                {
                    if (page == 1)
                    {
                        user.GoToCatalog();
                        log(string.Format("Пользователь {0} посетил каталог", i + 1));
                    }
                    else
                    {
                        user.GoToNextPage();
                        log(string.Format("Пользователь {0} перешел на страницу {1}", i + 1, page));
                    }

                    int? goodPos = user.ChoosenGoodPosition(page, pagePositionsCount);
                    //пользователь выбрал товар
                    if (goodPos != null)
                    {
                        
                        var goods = user.SeenGoods();
                        for (int j = 0; j < 20 && (goods = user.SeenGoods()).Count < 10; ++j)
                        {
                            Thread.Sleep(100);
                        }
                        try
                        {
                            goods[(int)goodPos - 1].Click();
                            log(string.Format("Пользователь {0} выбрал товар {1}", i + 1, (int)goodPos));
                        }
                        catch (Exception ex) {; }
                        
                        page = 99; //условие выхода
                    }
                    page++;
                }
                log(string.Format("Пользователь {0} покинул магазин", i + 1));

                user.Clear();
            }
            
            user.Close();
            log(string.Format("Тестирование закончено {0}", DateTime.Now.ToString("yyyy.MM.dd_H:mm:ss")));
            try
            {
                File.WriteAllText(
                    string.Format(@"C:\arc_experiment_{0}.log", DateTime.Now.ToString("yyyy.MM.dd_H:mm:ss")),
                    logStr.ToString());
            }
            catch(Exception ex)
            {
                Console.WriteLine("При попытке записать файл логов возникла ошибка");
            }
            
            Console.ReadKey();
        }

        static void log(string msg)
        {
            Console.WriteLine(msg);
            logStr.Append(msg + Environment.NewLine);
        }

        
    }
}
