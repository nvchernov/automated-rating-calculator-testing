﻿using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;

namespace ArcTesting
{
    public class User
    {
        private RemoteWebDriver driver;
        private string shopUrl = "";
        Random rnd = null;
        private int seed = 0;
        public int Seed { get { return seed; } }

        //страница, позиция
        private List<Tuple<int, int>> redGoods = new List<Tuple<int, int>>();
        //страница, позиция
        private List<Tuple<int, int>> blueGoods = new List<Tuple<int, int>>();

        public User(string shopUrl, int seed)
        {
            if (shopUrl == null || shopUrl == string.Empty)
                throw new ArgumentException();
            
            this.seed = seed;
            rnd = new Random(seed);
            this.shopUrl = shopUrl;
            driver = new FirefoxDriver();
        }

        public void AddRedGood(int page, int position)
        {
            redGoods.Add(new Tuple<int, int>(page, position));
        }

        public void AddBlueGood(int page, int position)
        {
            blueGoods.Add(new Tuple<int, int>(page, position));
        }

        public void GoToCatalog()
        {
            driver.Navigate().GoToUrl(shopUrl);
        }

        public void GoToNextPage()
        {
            var list = driver.FindElementsById("navigation_1_next_page");
            if(list != null || list.Count > 0)
            {
                for (int j = 0; j < 20 && (list = driver.FindElementsById("navigation_1_next_page")).Count == 0; ++j)
                {
                    Thread.Sleep(100);
                }
                try
                {
                    list[0].Click();
                }catch(Exception ex) { }
            }
        }

        public List<Good> SeenGoods()
        {
            return driver.FindElementsByCssSelector("div.bx_catalog_item")
            .Select(x =>
            {
                return new Good(
                    x.Text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None)[0],
                    x
                );
            })
            .ToList();
        }

        public int? ChoosenGoodPosition(int catalogPage, int pagePositionsCount)
        {
            for(int i = 0; i < pagePositionsCount; ++i)
            {
                //вероятноть*100 клика на позицию
                double prob = 34.162f / ((catalogPage - 1) * pagePositionsCount + i + 1) - 0.6359f;
                //рандомное число
                var rndVal = (double)rnd.Next(0, 100 * 1000 * 1000) / (1000f * 1000f);
                if(redGoods.Exists(x => x.Item1 == catalogPage && x.Item2 == i + 1))
                {
                    prob *= 2f;
                }
                if (blueGoods.Exists(x => x.Item1 == catalogPage && x.Item2 == i + 1))
                {
                    prob /= 2f;
                }
                if (prob > rndVal)
                {
                    return i + 1;
                }
            }

            return null;
        }

        public bool WantToGoToNextPage(int curPage)
        {
            if (curPage < 1 || curPage > 5)
                return false;
            //вероятноть*100 клика на позицию
            double[] probs = new double[] { 71.33f, 3.99f, 1.6f, 1.2f, 1.01f };
            //рандомное число
            var rndVal = (double)rnd.Next(0, 100 * 1000 * 1000) / (1000f * 1000f);

            if (probs[curPage - 1] > rndVal)
                return true;

            return false;
        }

        public void Clear()
        {
            driver.Manage().Cookies.DeleteAllCookies();
        }

        public void Close()
        {
            driver.Close();
        }
    }
}
